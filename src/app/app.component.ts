import { Component } from '@angular/core';
import { Router, NavigationEnd, RouteReuseStrategy } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {

  currentRoute: string = "";
  name = 'Get Current Url Route Demo';

  constructor(router: Router) {
    this.currentRoute = router.url;
  };
}