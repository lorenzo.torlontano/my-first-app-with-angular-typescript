import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceGetPostDetailsService {

  private _url: string = "https://jsonplaceholder.typicode.com/posts/";

  constructor(private http: HttpClient) { }

  getPostDetails(id: string): Observable<any> {
    return this.http.get<any>(this._url + id)
  }
}
