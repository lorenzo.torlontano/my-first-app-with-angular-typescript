import { ServiceService } from '../../services/service.service';
import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

  public posts: any = []
  constructor(private _postService: ServiceService) { }

  ngOnInit(): void {
    this._postService.getPost().subscribe((re: any) => {
      this.posts.push(re)
    },
      err => console.log('HTTP Error', err),
      () => console.log('HTTP request completed.'));
    console.log("post >", this.posts);
  }

  consoleLog(val: any) {
    console.log(val)
  }
}
