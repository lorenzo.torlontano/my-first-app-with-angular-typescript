import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ServiceGetPostDetailsService } from 'src/app/services/service-get-post-details.service';

@Component({
  selector: 'app-post-details',
  templateUrl: './post-details.component.html',
  styleUrls: ['./post-details.component.css']
})
export class PostDetailsComponent implements OnInit {
  idFromUrl: string | null | undefined;
  public postsDetails: any = []

  constructor(
    private route: ActivatedRoute,
    private _postDetailService: ServiceGetPostDetailsService
  ) { }

  ngOnInit(): void {
    this.idFromUrl = String(this.route.snapshot.paramMap.get('id'));
    this._postDetailService.getPostDetails(this.idFromUrl).subscribe((res) => {
      this.postsDetails.push(res);
      this.postsDetails = this.postsDetails[0]
      console.log(this.postsDetails)
    },
      err => console.log('HTTP Error', err),
      () => console.log('HTTP request completed.'));
  };
}
