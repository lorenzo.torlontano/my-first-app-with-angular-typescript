import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.css']
})
export class ListPostsComponent implements OnInit {

  constructor(private router: Router) {
  }

  @Input() post: any

  value: number = 10;

  browseToDetails(id: number) {
    console.log(id);
    this.router.navigateByUrl(`/post/${id}`);
  }

  handleShowMore(value: number) {
    this.value = value
  }

  consoleLog(val: number): void {
    console.log(val)
  }

  ngOnInit(): void {
  }
}
